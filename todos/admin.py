from .models import Todo
from django.contrib import admin

# todos/admin.py
class TodoAdmin(admin.ModelAdmin):

    list_display = (
        "title",
        "body",
    )
admin.site.register(Todo, TodoAdmin)
